using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using PX.Common;
using PX.Data;

using PX.Objects.AR;
using PX.Objects.CM;
using PX.Objects.CA;
using PX.Objects.Common;
using PX.Objects.Common.Extensions;
using PX.Objects.Common.Tools;
using PX.Objects.Common.Bql;
using PX.Objects.CS;
using PX.Objects.GL;
using PX.Objects.GL.FinPeriods;
using PX.Objects.CR;
using PX.Objects.AP.MigrationMode;
using PX.Objects.CM.Standalone;
using PX.Objects.Common.GraphExtensions.Abstract;
using PX.Objects.Common.GraphExtensions.Abstract.DAC;
using PX.Objects.Common.GraphExtensions.Abstract.Mapping;
using PX.Objects.EP;
using PX.Objects.GL.FinPeriods.TableDefinition;
using PX.Objects.IN;
using PX.Objects.PM;
using Utilities = PX.Objects.Common.Utilities;
using PX.Objects.Common.Utility;
using PX.Data.WorkflowAPI;
using PX.SM;
using PX.Objects;
using PX.Objects.AP;

namespace NTCustomizations.Graph.Extensions
{

    public class APPaymentEntry_Ext : PXGraphExtension<APPaymentEntry>
    {
        #region Event Handlers

        protected void APAdjust_AdjdRefNbr_FieldUpdated(PXCache cache, PXFieldUpdatedEventArgs e)
        {

            var row = (APAdjust)e.Row;
            if (row.AdjdRefNbr != null)
            { //obtain apsubid
                APInvoiceEntry apinvoicegraph = PXGraph.CreateInstance<APInvoiceEntry >();
                var apinvoice = new APInvoice ();
                //JH changed on 6/21/18 to take out CU type due to customer can be VC
                // custacct = (BAccount)PXSelect<BAccount, Where<BAccount.type, Equal<constantCU>, And<BAccount.bAccountID, Equal<Required<BAccount.bAccountID>>>>>.Select(customerMaint, custID);
                apinvoice = (APInvoice)PXSelect<APInvoice , Where<APInvoice.refNbr, Equal<Required<APInvoice.refNbr>>>>.Select(apinvoicegraph, row.AdjdRefNbr );
                if (apinvoice != null) Base.CurrentDocument.Cache.SetValueExt<APPayment.aPSubID>(Base.CurrentDocument.Current, apinvoice.APSubID);

            }
        }


   


        protected void APPayment_APSubID_FieldDefaulting(PXCache cache, PXFieldDefaultingEventArgs e)
        {

            var row = (APPayment)e.Row;
          //  e.Cancel = true;

        }

        protected void APPayment_APSubID_FieldUpdating(PXCache cache, PXFieldUpdatingEventArgs e)
        {

            var row = (APPayment)e.Row;
              if (row == null) return;

        //   if (row.RefNbr != null) return;
            if (row.VendorID == null) return;
            //obtain the branchid
            PX.Objects.GL.Branch b = PXSelect<PX.Objects.GL.Branch, Where<PX.Objects.GL.Branch.branchID, Equal<Current<AccessInfo.branchID>>>>.Select(Base);
            PXTrace.WriteInformation(b.BranchCD.Trim());

            //sub needs to end with 40
        
            //   PXTrace.WriteInformation(row.APSubID.ToString());
            if (b.BranchCD.Trim() == "AMG")
            {
                //sub needs to end with 40
                Sub s2 = PXSelect<Sub, Where<Sub.subCD, Equal<Required<Sub.subCD>>>>.Select(Base, "0000040");
                if (row.APSubID == null || row.RefNbr == null || row.RefNbr == "") e.NewValue = s2.SubID;

            }
            if (b.BranchCD.Trim() == "APS")
            {
                //sub needs to end with 10
                Sub s2 = PXSelect<Sub, Where<Sub.subCD, Equal<Required<Sub.subCD>>>>.Select(Base, "0000010");
                if (row.APSubID == null || row.RefNbr == null || row.RefNbr == "") e.NewValue = s2.SubID;

            }
      
            if (b.BranchCD.Trim() == "WFE")
            {
                Sub s2 = PXSelect<Sub, Where<Sub.subCD, Equal<Required<Sub.subCD>>>>.Select(Base, "0000030");
                if (row.APSubID == null || row.RefNbr == null || row.RefNbr == "") e.NewValue = s2.SubID;

            }
            if (b.BranchCD.Trim() == "WFU")
            {
                Sub s2 = PXSelect<Sub, Where<Sub.subCD, Equal<Required<Sub.subCD>>>>.Select(Base, "0000020");
                if (row.APSubID == null || row.RefNbr == null || row.RefNbr == "") e.NewValue = s2.SubID;

            }
        }
        #endregion

    }
}